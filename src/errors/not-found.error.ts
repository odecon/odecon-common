import { BaseError } from "./base.error"
import { Severity } from "./common/severity"
import { StatusCode } from "./common/status-codes"
import { CollectionOf } from "@tsed/schema"
import { ErrorDescription } from "./common/error-description"

export class NotFoundError extends BaseError {
    constructor(description = "Not found", severity = Severity.WARNING, errorDescriptions: ErrorDescription[] = []) {
        super("NotFoundError", StatusCode.NOT_FOUND, description, severity)
        this.errors = errorDescriptions
    }

    @CollectionOf(ErrorDescription)
    public errors: ErrorDescription[]
}
