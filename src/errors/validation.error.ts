import { BaseError } from "./base.error"
import { StatusCode } from "./common/status-codes"
import { Severity } from "./common/severity"
import { CollectionOf } from "@tsed/schema"
import { ErrorDescription } from "./common/error-description"

export class ValidationError extends BaseError {
    constructor(
        description = "Validation failed",
        severity = Severity.WARNING,
        validationErrors: ErrorDescription[] = [],
    ) {
        super("ValidationError", StatusCode.BAD_REQUEST, description, severity)
        this.errors = validationErrors
    }

    @CollectionOf(ErrorDescription)
    public errors: ErrorDescription[]

    public addValidationError = (validationError: ErrorDescription): void => {
        this.errors.push(validationError)
    }
}
