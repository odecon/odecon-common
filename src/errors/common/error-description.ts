import { Enum, Property } from "@tsed/schema"

export enum Location {
    BODY = "body",
    HEADER = "header",
}

export class ErrorDescription {
    constructor(message: string, location: Location | undefined, parameter: string) {
        this.message = message
        this.location = location
        this.parameter = parameter
    }

    @Property()
    public message: string

    @Enum(Location)
    public location: Location | undefined

    @Property()
    public parameter: string
}
