export enum Severity {
    /**
     * Used when the error is expected and can be
     * handled by the application and typically the client
     * making the request.
     */
    WARNING = "WARNING",

    /**
     * Used when the error is not expected, but can
     * be handled by the application and typically the
     * client making the request.
     *
     * Errors with severity == ERROR is typically mapped
     * from unexpected errors that is accounted for, e.g.
     * using a catch. This can often be the case when a
     * request wrongfully has passed validation.
     *
     * Such errors will not break the state of the
     * application, but action is needed to prevent this error
     * from being thrown again
     */
    ERROR = "ERROR",

    /**
     * Used when the error is not expected and can not
     * be handled by the application nor the client
     * making the request.
     *
     * Such errors are typically thrown by some third
     * party library and not accounted for by the
     * implemented application. Action should be
     * taken in order to prevent such errors from
     * happening again.
     */
    CRITICAL = "CRITICAL",
}
