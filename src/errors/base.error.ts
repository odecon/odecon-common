import { StatusCode } from "./common/status-codes"
import { Severity } from "./common/severity"
import { Enum, Property } from "@tsed/schema"

export class BaseError extends Error {
    @Property()
    public readonly message: string

    @Property()
    public readonly name: string

    @Enum(StatusCode)
    public readonly status: StatusCode

    @Enum(Severity)
    public readonly severity: Severity

    constructor(name: string, status: StatusCode, description: string, severity: Severity) {
        super()
        Object.setPrototypeOf(this, new.target.prototype)

        this.name = name
        this.status = status
        this.severity = severity
        this.message = description

        Error.captureStackTrace(this)
    }
}
